-- carga catalogo de roles
INSERT INTO cv_gestor.roles
(`name`,`description`,`active`,`created_at`)
values
('Suscriptor','Suscriptor',1,now()),
('Administrador','Administrador',1,now());

-- carga catalogo de rangos
INSERT INTO cv_gestor.ranges
(`name`,`description`,`active`,`created_at`)
values
('$5000-$10000','$5000-$10000',1,now()),
('$10000-$15000','$10000-$15000',1,now()),
('$15000-$20000','$15000-$20000',1,now()),
('$20000-$30000','$20000-$30000',1,now()),
('$30000-$50000','$30000-$50000',1,now()),
('mas de $50000','mas de $50000',1,now());