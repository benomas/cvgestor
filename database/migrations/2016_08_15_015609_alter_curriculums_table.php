<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCurriculumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('curriculums', function ($table)
        {
            $table->integer('users_id')->unsigned();
            $table->integer('ranges_id')->unsigned();
            $table->foreign('users_id')->references('id')
            ->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('ranges_id')->references('id')
            ->on('ranges')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('curriculums', function ($table)
        {
            $table->dropForeign('curriculums_users_id_foreign');
            $table->dropForeign('curriculums_ranges_id_foreign');
            $table->dropColumn('users_id');
            $table->dropColumn('ranges_id');
        });
    }
}
