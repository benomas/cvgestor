<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAcademicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('academics', function ($table)
        {
            $table->integer('curriculums_id')->unsigned();
            $table->foreign('curriculums_id')->references('id')
            ->on('curriculums')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('academics', function ($table)
        {
            $table->dropForeign('academics_curriculums_id_foreign');
            $table->dropColumn('curriculums_id');
        });
    }
}
