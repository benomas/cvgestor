<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('works', function ($table)
        {
            $table->integer('curriculums_id')->unsigned();
            $table->foreign('curriculums_id')->references('id')
            ->on('curriculums')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('works', function ($table)
        {
            $table->dropForeign('works_curriculums_id_foreign');
            $table->dropColumn('curriculums_id');
        });
    }
}
