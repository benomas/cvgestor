/*var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify');*/
var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix)
{
    mix.sass('app.scss');
    mix.copy('node_modules/bootstrap-sass/assets/fonts', 'public/fonts');
    mix.copy('node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js', 'public/js/bootstrap/bootstrap.min.js');
    mix.copy('node_modules/angular/angular.min.js', 'public/js/angular/angular.min.js');
    mix.copy('node_modules/angular-locale/angular-locale_es-mx.js', 'public/js/angular/angular-locale_es-mx.js');
    mix.copy('node_modules/angular-route/angular-route.min.js', 'public/js/angular/angular-route.min.js');
    mix.copy('node_modules/angular-sanitize/angular-sanitize.min.js', 'public/js/angular/angular-sanitize.min.js');
    mix.copy('node_modules/angular-filter/dist/angular-filter.min.js', 'public/js/angular/angular-filter.min.js');
    mix.copy('node_modules/jquery/dist/jquery.min.js', 'public/js/jquery/jquery.min.js');
    mix.copy('node_modules/angular-bootstrap/ui-bootstrap.min.js', 'public/js/angular/ui-bootstrap.min.js');
    mix.copy('resources/assets/js/angular/app', 'public/js/angular/app');

    mix.styles([
        'cv-gestor-styles.css',
        'extra-styles.css'
    ]);

     /*mix.scriptsIn('public/js/angular/app');*/

});
