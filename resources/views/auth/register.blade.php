<!-- resources/views/auth/register.blade.php -->
<div class="container">
    <div class="content">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">
                <div class="panel panel-default" >
                    <div class="panel-heading auth-panel-heading">
                        <h3 class="panel-title">{{ trans('cvgestor.registry') }}</h3>
                    </div>
                    <div class="panel-body" style="margin:10px;">
                        <div class="row">
                            <form   method="POST"
                                    action="/cvgestor/auth/register"
                                    class="form-horizontal"
                            >
                                {!! csrf_field() !!}

                                <div class="form-group">
                                    <label class="col-xs-12 col-sm-4 col-lg-3" for="name">{{ trans('cvgestor.name') }}:</label>
                                    <div class="col-xs-12 col-sm-8 col-lg-9">
                                        <input  type="text"
                                                class="form-control"
                                                id="name"
                                                placeholder="{{ trans('cvgestor.name') }}"
                                                value="{{ old('name') }}"
                                                name="name"
                                        >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label  class="col-xs-12 col-sm-4 col-lg-3" for="email">{{ trans('cvgestor.email') }}:</label>
                                    <div class="col-xs-12 col-sm-8 col-lg-9">
                                        <input  type="email"
                                                class="form-control"
                                                id="email"
                                                placeholder="{{ trans('cvgestor.email') }}"
                                                value="{{ old('email') }}"
                                                name="email"
                                        >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label  class="col-xs-12 col-sm-4 col-lg-3" for="email">{{ trans('cvgestor.role') }}:</label>
                                    <div class="col-xs-12 col-sm-8 col-lg-9">
                                        <select name="role"
                                                id="role"
                                                placeholder="{{ trans('cvgestor.role') }}"
                                                value="{{ old('rol') }}"
                                                class="form-control"
                                        >
                                            @foreach ($roles_option as $values)
                                                <option value="{{$values->value}}">{{ $values->label }}</option >
                                             @endforeach
                                        <select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-xs-12 col-sm-4 col-lg-3" for="password">{{ trans('cvgestor.password') }}:</label>
                                    <div class="col-xs-12 col-sm-8 col-lg-9">
                                        <input  type="password"
                                                class="form-control"
                                                id="password"
                                                placeholder="Password"
                                                value="{{ old('password') }}"
                                                name="password"
                                        >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-xs-12 col-sm-4 col-lg-3" for="password_confirmation">{{ trans('cvgestor.password_confirm') }}:</label>
                                    <div class="col-xs-12 col-sm-8 col-lg-9">
                                        <input  type="password"
                                                class="form-control"
                                                id="password_confirmation"
                                                placeholder=" Confirm Password:"
                                                value="{{ old('password_confirmation') }}"
                                                name="password_confirmation"
                                        >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-xs-offset-3 col-xs-6">
                                      <button type="submit" class="btn btn-default btn-block">{{ trans('cvgestor.register') }}</button>
                                    </div>
                                </div>

                            </form>
                            <a href="login">{{ trans('cvgestor.login') }}</a>
                        </div>
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>