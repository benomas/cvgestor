<!DOCTYPE html>
<html>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <base href="/cvgestor/">
        <!--<link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">-->
        <link href="css/app.css" rel="stylesheet" type="text/css">
        <link href="css/all.css" rel="stylesheet" type="text/css">
        <script src="js/angular/angular.min.js"></script>
        <script src="js/angular/angular-locale_es-mx.js"></script>
        <script src="js/angular/angular-route.min.js"></script>
        <script src="js/angular/angular-sanitize.min.js"></script>
        <script src="js/angular/angular-filter.min.js"></script>
        <script src="js/jquery/jquery.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>

        <script src="js/angular/app/cv-gestor-app.js"></script>

        <script src="js/angular/app/controllers/dashboard-controllers.js"></script>

        <script src="js/angular/app/services/user-auth-services.js"></script>
        <script src="js/angular/app/services/curriculum-services.js"></script>
        <script src="js/angular/app/services/academic-services.js"></script>
        <script src="js/angular/app/services/work-services.js"></script>
        <script src="js/angular/app/services/catalog-services.js"></script>

        <script src="js/angular/app/directives/curriculum-directives.js"></script>
        <script src="js/angular/app/directives/academic-directives.js"></script>
        <script src="js/angular/app/directives/work-directives.js"></script>
        <script src="js/angular/app/directives/general-directives.js"></script>

        <script>
        var site_url='cvgestor/';
        var api_url='/cvgestor/';
        var angular_url='js/angular/app/templates/';
        var trans = JSON.parse('{!! $jsTrans !!}');
        var api_token={"_token":"{{ csrf_token() }}"};

        function jsTrans(index,value)
        {
            if(typeof trans[index][value]!=='undefined')
                return trans[index][value];
            return value;
        }

        </script>
    </head>
    <body >
        <div class="container">
            <div class="content" ng-app="cvGestorApp">
                <div ng-view>
                </div>
            </div>
        </div>
    </body>
</html>
