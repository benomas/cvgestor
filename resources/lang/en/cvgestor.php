<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'name'                  => 'Name',
    'email'                 => 'Electronic mail',
    'role'                  => 'Role',
    'password'              => 'Password',
    'password_confirm'      => 'Password confirmation',
    'registry'              => 'Registry',
    'register'              => 'Register',
    'login'                 => 'Login',
    'logout'                => 'Logout',
    'remember_me'           => 'Remember me',
    'no_login'              => 'No login',
    'welcome'               => 'Welcome',
    'cv_list'               => 'Curriculum list',
    'age'                   => 'Age',
    'range'                 => 'Range',
    'action'                => 'Action',
    'academic_begin_year'   => 'Academi begin year',
    'academic_end_year'     => 'Academi end year',
    'institute'             => 'Institute',
    'career'                => 'Career',
    'work_begin_year'       => 'Work begin year',
    'work_end_year'         => 'Work begin year',
    'company'               => 'Company',
    'work_position'         => 'Work position',
    'academic_information'  => 'Academic information',
    'work_information'      => 'Work information',
    'cv_detail'             => 'Curriculum detail',
    'general_information'   => 'General information',
    'togle_title'           => 'Click for Show/hide',
    'close'                 => 'Close',
    'position'              => 'Position',
    'new'                   => 'New',
    'save'                  => 'Save',
    'update'                => 'Update',
    'delete'                => 'Delete',
    'generic_error_message' => 'The action cant been completed',
    'create_curriculum'     => 'Create curriculum'

];
