<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'name'                  => 'Nombre',
    'email'                 => 'Correo electronico',
    'role'                  => 'Rol',
    'password'              => 'Contraseña',
    'password_confirm'      => 'Confirmación de contraseña',
    'registry'              => 'Registro',
    'register'              => 'Registrar',
    'login'                 => 'Ingresar',
    'logout'                => 'Cerrar sesión',
    'remember_me'           => 'Recordarme',
    'no_login'              => 'No a ingresado ningun usuario',
    'welcome'               => 'Bienvenido',
    'cv_list'               => 'Listado de curriculums',
    'age'                   => 'Edad',
    'range'                 => 'Rango',
    'action'                => 'Acción',
    'academic_begin_year'   => 'Año de ingreso',
    'academic_end_year'     => 'Año de egreso',
    'institute'             => 'Institución',
    'career'                => 'Carrera',
    'work_begin_year'       => 'Año de ingreso',
    'work_end_year'         => 'Año de egreso',
    'company'               => 'Compañia',
    'work_position'         => 'Puesto',
    'academic_information'  => 'Formación academica',
    'work_information'      => 'Experiencia laboral',
    'cv_detail'             => 'Detalle de curriculum',
    'general_information'   => 'Información general',
    'togle_title'           => 'Click para Ocultar/Mostrar',
    'close'                 => 'Cerrar',
    'position'              => 'Posición',
    'new'                   => 'Nuev@',
    'save'                  => 'Guardar',
    'update'                => 'Actualizar',
    'delete'                => 'Borrar',
    'generic_error_message' => 'No se ha podido completar la acción, intente nuevamente',
    'create_curriculum'     => 'Crear curriculum'

];