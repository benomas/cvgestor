// Factory, donde estan todos los servicios.

userAuth.factory('userAuthServices', ['$http', function($http)
{
    // api_url se define como variable global en vista main.blade
    var objectApi = 'user';
    var userAuthServices = {};

    userAuthServices.showCurrentUser = function ()
    {
        return $http.get(api_url + objectApi + '/currentUser');
    };

    userAuthServices.index = function ()
    {
        return $http.get(api_url + objectApi);
    };

    userAuthServices.show = function (id)
    {
        return $http.get(api_url + objectApi+'/'+id);
    };

    userAuthServices.store = function (user)
    {
        return $http.post(api_url + objectApi , user);
    };

    userAuthServices.update = function (datos)
    {
        return $http.put(api_url + objectApi , user);
    };

    userAuthServices.destroy = function (id)
    {
        return $http.delete(api_url + objectApi+'/'+id);
    };

    return userAuthServices;
}]);