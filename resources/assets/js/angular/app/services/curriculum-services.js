// Factory, donde estan todos los servicios.

curriculum.factory('curriculumServices', ['$http', function($http)
{
    // api_url se define como variable global en vista main.blade
    var objectApi = 'curriculum';
    var curriculumServices = {};

    curriculumServices.index = function ()
    {
        return $http.get(api_url + objectApi);
    };

    curriculumServices.show = function (id)
    {
        return $http.get(api_url + objectApi+'/'+id);
    };

    curriculumServices.store = function (data)
    {
        return $http.post(api_url + objectApi , data);
    };

    curriculumServices.update = function (data,index,id)
    {
        return $http.put(api_url + objectApi + '/' +id, data);
    };

    curriculumServices.destroy = function (id)
    {
        return $http.delete(api_url + objectApi+'/'+id);
    };

    curriculumServices.index_by_user = function (user_id)
    {
        return $http.get(api_url + objectApi + '/user_id/' + user_id);
    };


    return curriculumServices;
}]);