restrict: "A";

academic.directive('academicList', ['academicServices',function (academicServices)
{
    return {
                restrict:'E',
                scope:
                {
                    currentCurriculum:"=",
                    currentUser:"="
                },
                templateUrl:angular_url + 'academic-list.html',
                link: function(scope, element, attrs)
                {
                    scope.jsTrans=jsTrans;
                    scope.academicsData={"academics":[]};
                    scope.newAcademic={};
                    scope.newAcademicError={};
                    scope.academicsError=[];
                    scope.statusCrud=[];

                    //Obtiene un registro academico por id
                    scope.showacademic = function (id)
                    {
                      academicServices.show(id)
                      .success(function(response)
                      {
                        if(Object.keys(response).length > 0)
                        {
                          scope.academicsData['currentAcademic'] = response.data;
                        }
                        else
                          delete scope.academicsData;
                      }).error(function(error)
                      {
                            alert(scope.jsTrans('cvgestor','generic_error_message'));
                      });
                    };

                    //Obtiene todos los academics
                    scope.showAcademicsByCurriculum = function (curriculums_id)
                    {
                        academicServices.index_by_curriculum(curriculums_id)
                        .success(function(response)
                        {
                            if(Object.keys(response).length > 0)
                            {
                                scope.academicsData['academics'] = response.data;
                            }
                            else
                            {
                                delete scope.academicsData['academics'];
                            }
                        }).error(function(error)
                        {
                        });
                    };

                    scope.$watch('currentCurriculum.id',function()
                    {
                        if(typeof scope.currentCurriculum==='object' && scope.currentCurriculum)
                            scope.showAcademicsByCurriculum(scope.currentCurriculum.id);
                    },true);

                    scope.saveNewAcademic = function()
                    {
                        if(typeof scope.currentCurriculum!=='undefined')
                        {
                            scope.newAcademic.curriculums_id=scope.currentCurriculum.id;
                            academicServices.store(scope.newAcademic)
                            .success(function(response)
                            {
                                if(Object.keys(response).length > 0)
                                {
                                    scope.academicsData['academics'].push(response.data);
                                    scope.newAcademic={};
                                    scope.newAcademicError={};
                                }
                            }).error(function(error)
                            {
                                scope.newAcademicError=error.data;
                            });
                        }
                    }

                    scope.deleteAcademic = function(index)
                    {

                        if( typeof scope.academicsData!=='undefined' &&
                            typeof scope.academicsData['academics']!=='undefined' &&
                            typeof scope.academicsData['academics'][index]!=='undefined'
                            )
                        {
                            academicServices.destroy(scope.academicsData['academics'][index].id)
                            .success(function(response)
                            {
                                scope.academicsData['academics'].splice(index,1);
                            }).error(function(error)
                            {
                                alert(scope.jsTrans('cvgestor','generic_error_message'));
                            });
                        }
                    }

                    scope.editionMode = function(index)
                    {
                        scope.statusCrud[index]='update';
                    }

                    scope.completeEdition = function(index)
                    {
                        if(typeof scope.currentCurriculum!=='undefined')
                        {
                            academicServices.update(scope.academicsData['academics'][index],index,scope.academicsData['academics'][index].id)
                            .success(function(response)
                            {
                                if(Object.keys(response).length > 0)
                                {
                                    scope.statusCrud[index]='show';
                                    scope.academicsError[index]={};
                                }
                            }).error(function(error)
                            {
                                scope.academicsError[index]=error.data;
                            });
                        }
                    }

                }
            }
}]);