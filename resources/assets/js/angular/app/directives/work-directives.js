restrict: "A";

work.directive('workList', ['workServices',function (workServices)
{
    return {
                restrict:'E',
                scope:
                {
                    currentCurriculum:"=",
                    currentUser:"="
                },
                templateUrl:angular_url + 'work-list.html',
                link: function(scope, element, attrs)
                {
                    scope.jsTrans=jsTrans;
                    scope.worksData={"works":[]};
                    scope.newWork={};
                    scope.newWorkError={};
                    scope.worksError=[];
                    scope.statusCrud=[];

                    //Obtiene un registro worko por id
                    scope.showWork = function (id)
                    {
                      workServices.show(id)
                      .success(function(response)
                      {
                        if(Object.keys(response).length > 0)
                        {
                          scope.worksData['currentWork'] = response.data;
                        }
                        else
                          delete scope.worksData;
                      }).error(function(error)
                      {
                        alert(scope.jsTrans('cvgestor','generic_error_message'));
                      });
                    };

                    //Obtiene todos los works
                    scope.showWorksByCurriculum = function (curriculums_id)
                    {
                      workServices.index_by_curriculum(curriculums_id)
                      .success(function(response)
                      {
                        if(Object.keys(response).length > 0)
                        {
                          scope.worksData['works'] = response.data;
                        }
                        else
                          delete scope.worksData['works'];
                      }).error(function(error)
                      {
                      });
                    };


                    scope.$watch('currentCurriculum.id',function()
                    {
                        if(typeof scope.currentCurriculum==='object' && scope.currentCurriculum)
                            scope.showWorksByCurriculum(scope.currentCurriculum.id);
                    },true);

                    scope.saveNewWork = function()
                    {
                        if(typeof scope.currentCurriculum!=='undefined')
                        {
                            scope.newWork.curriculums_id=scope.currentCurriculum.id;
                            workServices.store(scope.newWork)
                            .success(function(response)
                            {
                                if(Object.keys(response).length > 0)
                                {
                                  scope.worksData['works'].push(response.data);
                                  scope.newWork={};
                                  scope.newWorkError={};
                                }
                            }).error(function(error)
                            {
                                scope.newWorkError=error.data;
                            });
                        }
                    }

                    scope.deleteWork = function(index)
                    {

                        if( typeof scope.worksData!=='undefined' &&
                            typeof scope.worksData['works']!=='undefined' &&
                            typeof scope.worksData['works'][index]!=='undefined'
                            )
                        {
                            workServices.destroy(scope.worksData['works'][index].id)
                            .success(function(response)
                            {
                                scope.worksData['works'].splice(index,1);
                            }).error(function(error)
                            {
                                alert(scope.jsTrans('cvgestor','generic_error_message'));
                            });
                        }
                    }

                    scope.editionMode = function(index)
                    {
                        scope.statusCrud[index]='update';
                    }

                    scope.completeEdition = function(index)
                    {
                        if(typeof scope.currentCurriculum!=='undefined')
                        {
                            workServices.update(scope.worksData['works'][index],index,scope.worksData['works'][index].id)
                            .success(function(response)
                            {
                                if(Object.keys(response).length > 0)
                                {
                                    scope.statusCrud[index]='show';
                                    scope.worksError[index]={};
                                }
                            }).error(function(error)
                            {
                                scope.worksError[index]=error.data;
                            });
                        }
                    }

                }
            }
}]);