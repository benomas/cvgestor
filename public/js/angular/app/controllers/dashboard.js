'use strict';


dashboard.controller('dashboardController',
[ '$scope','$location','userAuthService',
  function($scope,$location,userAuthService)
  {
    $scope.currentUser={};
    $scope.jsTrans=jsTrans;

    $scope.showCurrentUser = function ()
    {
      userAuthService.showCurrentUser()
      .success(function(response)
      {
        if(Object.keys(response).length > 0)
          $scope.currentUser = response.data;
        else
          delete $scope.currentUser;
        console.log($scope.currentUser);
      }).error(function(error)
      {
        console.log('error showCurrentUser');
      });
    };
    $scope.showCurrentUser();
  }
]);


