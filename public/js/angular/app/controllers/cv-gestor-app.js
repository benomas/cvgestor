'use strict';

  var cvGestorApp =  angular.module("cvGestorApp",
    ['ngRoute',
    'ngSanitize',
    'angular.filter',
    'userAuth',
    'dashboard']);

    //----Routing & multiples Views
    cvGestorApp.config(['$routeProvider',
    function($routeProvider)
    {
      $routeProvider.when('/dashboard/',
      {
        templateUrl: 'templates/dashboard.html',
        controller: 'dashboardController'
      })
      .otherwise(
      {
        redirectTo: '/dashboard/'
      });
    }]);

//Aberracion de IE- No guardar cache para Ajax.
cvGestorApp.config(['$httpProvider', function($httpProvider) {
    //initialize get if not there
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }

    //disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = '0';
}]);

