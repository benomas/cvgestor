'use strict';


dashboard.controller('dashboardController',
[ '$scope','$location','userAuthServices','curriculumServices',
  function($scope,$location,userAuthServices,curriculumServices)
  {
    $scope.currentUser=null;
    $scope.jsTrans=jsTrans;
    $scope.curriculumData = {"curriculumInProgress":false,"currentCurriculum":{}};

    //Obtiene información del usuario actual
    $scope.showCurrentUser = function ()
    {
      userAuthServices.showCurrentUser()
      .success(function(response)
      {
        if(Object.keys(response).length > 0)
        {
          $scope.currentUser = response.data;
          if($scope.currentUser.role_name==='Administrador')
          {
            $scope.showCurriculums();
          }
          else
          {
            $scope.showCurriculum($scope.currentUser.id);
          }
        }
        else
          delete $scope.currentUser;
      }).error(function(error)
      {
      });
    };

    //Obtiene curriculum de un usuario
    $scope.showCurriculum = function (user_id)
    {
      curriculumServices.index_by_user(user_id)
      .success(function(response)
      {
        if(Object.keys(response).length > 0)
        {
          $scope.curriculumData['currentCurriculum'] = response.data;
           delete $scope.curriculumData['curriculumInProgress'];
        }
        else
          $scope.curriculumData['currentCurriculum'] = {};
      }).error(function(error)
      {
      });
    };

    //Obtiene todos los curriculums
    $scope.showCurriculums = function (user_id)
    {
      curriculumServices.index()
      .success(function(response)
      {
        if(Object.keys(response).length > 0)
        {
          $scope.curriculumData['curriculums'] = response.data;
          delete $scope.curriculumData['curriculumInProgress'];
        }
      }).error(function(error)
      {

      });
    };

    $scope.beginCurriculum = function()
    {
      $scope.curriculumData['curriculumInProgress']=true;
    }
    $scope.showCurrentUser();
  }
]);


