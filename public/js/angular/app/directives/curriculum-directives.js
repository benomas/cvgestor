restrict: "A";

curriculum.directive('curriculumList', ['curriculumServices',function (curriculumServices)
{
    return {
                restrict:'E',
                scope:
                {
                    curriculums:"=",
                    currentCurriculum:"="
                },
                templateUrl:angular_url + 'curriculum-list.html',
                link: function(scope, element, attrs)
                {
                    scope.jsTrans=jsTrans;
                    if(typeof scope.curriculums !=='undefined')
                        scope.totalOfCurriculums = scope.curriculums.length;

                    scope.setCurrentCurriculum = function(curriculum)
                    {
                        scope.currentCurriculum=curriculum;
                    }
                }
            }
}]);

curriculum.directive('curriculumView',  ['curriculumServices','rangeServices',function (curriculumServices,rangeServices)
{
    return {
                restrict:'E',
                scope:
                {
                    currentCurriculum:"=",
                    currentUser:"=",
                    curriculumInProgress:"="
                },
                templateUrl:angular_url + 'curriculum-view.html',
                link: function(scope, element, attrs)
                {
                    scope.jsTrans=jsTrans;
                    scope.curriculumsError=[];
                    scope.statusCrud=[];
                    scope.ranges=[];

                    scope.rangeIndex = function()
                    {
                        rangeServices.index()
                        .success(function(response)
                        {
                            if(Object.keys(response).length > 0)
                            {
                              scope.ranges = response.data;
                            }
                            else
                              delete scope.ranges;
                        }).error(function(error)
                        {
                            alert(scope.jsTrans('cvgestor','generic_error_message'));
                        });
                    }

                    //Obtiene un registro academico por id
                    scope.showCurriculum = function (id)
                    {
                      curriculumServices.show(id)
                      .success(function(response)
                      {
                        if(Object.keys(response).length > 0)
                        {
                          scope.currentCurriculum = response.data;
                        }
                        else
                          delete scope.currentCurriculum;
                      }).error(function(error)
                      {
                            alert(scope.jsTrans('cvgestor','generic_error_message'));
                      });
                    };


                    scope.editionMode = function(index)
                    {
                        scope.statusCrud[index]='update';
                    }

                    scope.completeEdition = function(index)
                    {
                        curriculumServices.update(scope.currentCurriculum,index,scope.currentCurriculum.id)
                        .success(function(response)
                        {
                            if(Object.keys(response).length > 0)
                            {
                                scope.statusCrud[index]='show';
                                scope.curriculumsError[index]={};
                            }
                        }).error(function(error)
                        {
                            scope.curriculumsError[index]=error.data;
                        });
                    }

                    scope.checkSelected = function(key, oldValue)
                    {
                        return key.toString()===oldValue.toString();
                    }

                    scope.saveNewCurriculum = function()
                    {
                        scope.currentCurriculum.users_id=scope.currentUser.id;
                        curriculumServices.store(scope.currentCurriculum)
                        .success(function(response)
                        {
                            if(Object.keys(response).length > 0)
                            {
                                scope.currentCurriculum=response.data;
                                scope.curriculumsError[0]={};
                                scope.curriculumInProgress=false;
                            }
                        }).error(function(error)
                        {
                            scope.curriculumsError[0]=error.data;
                        });
                    }

                    scope.unSetCurriculum=function()
                    {
                        scope.currentCurriculum={};
                    }
                    scope.rangeIndex();

                }
            }
}]);