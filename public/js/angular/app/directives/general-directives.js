restrict: "A";
//function for make relative selectors like folder navegation
function solveIdentation(htmlElement,identationString)
{
    var res;

    // if the identationString is empty
    if(identationString==='')
        return htmlElement;

    // check identiation for sibling element
    res = identationString.replace(/^\.\.\/[0-9]+/, "");
    if(res!== identationString)
    {
        var childIndex = identationString.replace(/^\.\.\/([0-9]+)(.|\s)*$/, "$1");
        childIndex = parseInt(childIndex);
        childIndex = childIndex *2 +1;
        return solveIdentation(htmlElement.parentNode.childNodes[childIndex],res);
    }

    // check identiation for parent element
    res = identationString.replace(/^\.\.\//, "");
    if(res!== identationString)
    {
        return solveIdentation(htmlElement.parentNode,res);
    }

    // check identiation for child element
    res = identationString.replace(/^\/[0-9]+/, "");
    if(res!== identationString)
    {
        var childIndex = identationString.replace(/^\/([0-9]+)(.|\s)*$/, "$1");
        childIndex = parseInt(childIndex);
        childIndex = childIndex *2 +1;
        return solveIdentation(htmlElement.childNodes[childIndex],res);
    }

    // return original element if not matches
    return htmlElement;
};

/**
 * description      directive for animated hide and show element
 * @author          Benomas  (benomas@gmail.com) 2015
 * @access          dependencies none
 * @param          type param. toggler json with config:
 * @param          closedIcon       is html for close icon status
 * @param          openIcon     is html for close icon status
 * @param          toggleStatus is currentStatus (parsed this param is like initical status)
 * @param          actionLauncher   is identation string that define element that triger de action
 * @param          actionTarjet is identation string that define element to be apply with the action
 * @param          actionShower is identation string that define element to append action status
 */
general.directive('toggler',['$timeout', function($timeout)
{
    return {
                restrict:'A',
                scope:
                {
                },
                link: function(scope, element, attrs)
                {
                    $timeout(function()
                    {
                        // html for closed status, has default
                        scope.closedIcon    = '<span class="glyphicon pull-right glyphicon-folder-close" ></span>';
                        // html for open status, has default
                        scope.openIcon      = '<span class="glyphicon pull-right glyphicon-folder-open" ></span>';
                        // html for actual status, has default
                        scope.toggleStatus  = 1;
                        // html for actual status, has default
                        scope.scrollPercent = 0;
                        // time for completed hide animation
                        scope.hideTime = 400;
                        // time for completed show animation
                        scope.showTime = 400;
                        // time for completed initial hide animation
                        scope.initialHideTime = 400;
                        // time for completed initial show animation
                        scope.initialShowTime = 400;
                        // html for title , has default
                        scope.title = "Ocultar/Mostrar";

                        // json with config
                        scope.params = angular.fromJson(attrs.toggler);

                        if(typeof scope.params.closedIcon !== 'undefined')
                            scope.closedIcon = scope.params.closedIcon;

                        if(typeof scope.params.openIcon !== 'undefined')
                            scope.openIcon = scope.params.openIcon;

                        if(typeof scope.params.showTime !== 'undefined')
                            scope.showTime = scope.params.showTime;

                        if(typeof scope.params.hideTime !== 'undefined')
                            scope.hideTime = scope.params.hideTime;

                        if(typeof scope.params.initialShowTime !== 'undefined')
                            scope.initialShowTime = scope.params.initialShowTime;

                        if(typeof scope.params.initialHideTime !== 'undefined')
                            scope.initialHideTime = scope.params.initialHideTime;

                        if(typeof scope.params.toggleStatus !== 'undefined')
                            scope.toggleStatus = scope.params.toggleStatus;
                        if((typeof scope.params.scrollPercent !== 'undefined' &&
                            scope.params.scrollPercent>=0 &&
                            scope.params.scrollPercent <=100) ||
                            scope.params.scrollPercent ===null
                            )
                            scope.scrollPercent = scope.params.scrollPercent;

                        if(typeof scope.params.title !== 'undefined')
                            scope.title = scope.params.title;


                        // process string identation to selected a element
                        scope.solveIdentation = solveIdentation;


                        // if actionLauncher defined get element else, set with current
                        if(typeof scope.params.actionLauncher !== 'undefined')
                            scope.launcher          = scope.solveIdentation(element[0],scope.params.actionLauncher);
                        else
                            scope.launcher = element[0];

                        // if actionTarjet defined get element else, set with current
                        if(typeof scope.params.actionTarjet !== 'undefined')
                            scope.tarjet            = scope.solveIdentation(element[0],scope.params.actionTarjet);
                        else
                            scope.tarjet = element[0];

                        // if actionShower defined get element else, set with current
                        if(typeof scope.params.actionShower !== 'undefined')
                            scope.shower            = scope.solveIdentation(element[0],scope.params.actionShower);
                        else
                            scope.shower = element[0];

                        // get angular object element
                        scope.launcherElement   = angular.element(scope.launcher);
                        scope.tarjetElement     = angular.element(scope.tarjet);
                        scope.showerElement     = angular.element(scope.shower);

                        scope.launcherElement   = $(scope.launcherElement);
                        scope.tarjetElement     = $(scope.tarjetElement);
                        scope.showerElement     = $(scope.showerElement);


                        // save original html from showerElement
                        scope.showerElementHtml = $(scope.showerElement).html();

                        if(scope.toggleStatus == 1)
                        {
                            scope.tarjetElement.slideDown(scope.initialShowTime);
                            $(scope.showerElement).html( scope.showerElementHtml + scope.openIcon);
                        }

                        if(scope.toggleStatus == 0)
                        {
                            scope.tarjetElement.slideUp(scope.initialHideTime);
                            $(scope.showerElement).html(scope.showerElementHtml + scope.closedIcon);
                        }

                        $(scope.launcherElement).attr('title',scope.title);
                        $(scope.launcherElement).addClass('togler-launcher-element');
                        // ejecute action on click
                        $(scope.launcherElement).click(function()
                        {
                            if(scope.toggleStatus == 1)
                            {
                                scope.toggleStatus = 0;
                                scope.tarjetElement.slideUp(scope.hideTime);
                                $(scope.showerElement).html(scope.showerElementHtml + scope.closedIcon);
                            }
                            else
                                if(scope.toggleStatus==0)
                                {
                                    scope.toggleStatus = 1;
                                    scope.tarjetElement.slideDown(scope.showTime,'swing',function()
                                    {
                                        if(scope.scrollPercent ===null)
                                            return false;
                                        var elementTop      = $(scope.tarjetElement).offset().top -120;// rest the menu heght;
                                        var elementBottom   = $(scope.tarjetElement).outerHeight(true);
                                        var percentElementBottom = parseInt(scope.scrollPercent * elementBottom / 100);
                                        var scrollTo = elementTop + percentElementBottom;
                                        $("html, body").scrollTop(scrollTo);
                                    });
                                    $(scope.showerElement).html(scope.showerElementHtml + scope.openIcon);
                                }
                        });

                    });
                }
            }
}]);


/**
 * descripcion  error v2
 * @author      Benomas  (benomas@gmail.com) 2015
 * @access      dependencias    $timeout
 * @param       tipo parametros     -
 */

general.directive('errorsWatcher', ['$compile','$timeout',function ($compile,$timeout)
{
    return {
                restrict: "E",
                scope:
                {
                    params:"="
                },
                template:'<div ng-if="hasErrors" class="alert-danger errors-watcher row" role="alert" >'+
                            '<div class="errors-watcher-repeat" ng-repeat="(key, value) in errors" ng-bind-html="bindError(value)" >'+
                            ''+
                            '</div>'+
                         '</div>',
                link:function(scope, element, attrs)
                {
                    $timeout(function()
                    {
                        if(typeof scope.params.fields=== 'undefined')
                            return;
                        scope.errors = {};
                        scope.hasErrors=false;
                        scope.bindError = function(value)
                        {
                            return '<span class="errors-icon glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>'+value;
                        }

                        scope.$watchCollection('params.errorObject',function()
                        {
                            scope.errors = {};
                            if( typeof scope.params!=='undefined' &&
                                typeof scope.params.errorObject!=='undefined' &&
                                typeof scope.params.errorObject.error!=='undefined')
                            {
                                $.each(scope.params.fields,function(index,value)
                                {
                                    if(typeof scope.params.errorObject.error[value]!== 'undefined')
                                        scope.errors[value] = scope.params.errorObject.error[value];
                                });
                            }
                            if(!$.isEmptyObject(scope.errors))
                                scope.hasErrors=true;
                            else
                                scope.hasErrors=false;
                        });

                    });

                }
            };
}]);

/**
 * descripcion  error v2
 * @author      Benomas  (benomas@gmail.com) 2015
 * @access      dependencias    $timeout
 * @param       tipo parametros     -
 */

general.directive('errorFieldManager', ['$compile','$timeout',function ($compile,$timeout)
{
    return {
                restrict: "EA",
                scope:
                {
                    errorFieldManager:"="
                },
                link:function(scope, element, attrs)
                {
                    $timeout(function()
                    {
                        if(typeof scope.errorFieldManager.fieldName=== 'undefined')
                            return;

                        scope.solveIdentation       = solveIdentation;
                        scope.hasBackgroundError    =  false;
                        scope.backgroundAdjustX     =  2;
                        scope.backgroundAdjustY     =  2;

                        scope.$watch('errorFieldManager.errorObject',function(newVal ,oldVal)
                        {
                            //console.log(scope.errorFieldManager.errorObject);
                            if( typeof scope.errorFieldManager!=='undefined' &&
                                typeof scope.errorFieldManager.errorObject!=='undefined' &&
                                typeof scope.errorFieldManager.errorObject.error!=='undefined' &&
                                typeof scope.errorFieldManager.errorObject.error[scope.errorFieldManager.fieldName]!== 'undefined' &&
                                newVal!==oldVal
                                )
                                scope.error=scope.errorFieldManager.errorObject.error[scope.errorFieldManager.fieldName];
                            else
                                scope.error=null;
                            if(scope.error)
                            {
                                if(typeof scope.newElementReference=== 'undefined')
                                    scope.newElementReference = {};
                                else
                                    scope.newElementReference.remove();

                                if(typeof scope.errorFieldManager.tarjetElement !=='undefined')
                                    scope.elementReference = scope.solveIdentation(element[0],scope.errorFieldManager.tarjetElement);
                                else
                                    scope.elementReference  = element;

                                scope.originalElementCssBorder = element.css('border');
                                $(scope.elementReference).addClass('element-width-error');
                                $(scope.elementReference).removeClass('element-widthout-error');
                                scope.newElementReference   = $compile('<error-detail ></error-detail>')(scope);


                                if($(scope.elementReference).is("input") && ($(scope.elementReference).prop("type")==='radio' || $(scope.elementReference).prop("type")==='checkbox'))
                                {
                                    scope.hasBackgroundError= true;
                                    scope.backgroundElement = $compile('<div class="error-background"></div>')(scope);
                                }

                                scope.recalculateDimension  = function()
                                {
                                    $(scope.newElementReference).width('-moz-min-content');
                                    $(scope.newElementReference).width('min-content');
                                    $(scope.newElementReference).width('-webkit-min-content');

                                    $(scope.newElementReference).css({"min-width":$(scope.elementReference).outerWidth()});

                                    $(scope.newElementReference).offset({left:$(scope.elementReference).offset().left,top:$(scope.elementReference).offset().top + $(scope.elementReference).outerHeight()});
                                    if(typeof scope.errorFieldManager.css !=='undefined')
                                        $(scope.newElementReference).css(scope.errorFieldManager.css);

                                    if(scope.hasBackgroundError)
                                    {
                                        $(scope.backgroundElement).width($(scope.elementReference).outerWidth() + scope.backgroundAdjustX *2 );
                                        $(scope.backgroundElement).height($(scope.elementReference).outerHeight() + scope.backgroundAdjustY *2 );
                                        $(scope.backgroundElement).offset({left:$(scope.elementReference).offset().left-scope.backgroundAdjustX,top:$(scope.elementReference).offset().top -scope.backgroundAdjustY});

                                        $(scope.elementReference).css( {"zIndex":2,"position":"relative"});
                                        $(scope.backgroundElement).css( "zIndex",  $(scope.elementReference).css( "zIndex") -1);
                                    }

                                };
                                scope.watchSize = function()
                                {
                                    $( window ).resize(function()
                                    {
                                        scope.$apply(function()
                                        {
                                            scope.recalculateDimension();
                                        });
                                    });
                                };
                                $(scope.elementReference).parent().append(scope.newElementReference);

                                if(scope.hasBackgroundError)
                                {
                                    $(scope.elementReference).parent().append(scope.backgroundElement);
                                    $(scope.backgroundElement).width($(scope.elementReference).outerWidth() + scope.backgroundAdjustX *2 );
                                    $(scope.backgroundElement).height($(scope.elementReference).outerHeight() + scope.backgroundAdjustY *2 );
                                    $(scope.backgroundElement).offset({left:$(scope.elementReference).offset().left-scope.backgroundAdjustX,top:$(scope.elementReference).offset().top -scope.backgroundAdjustY});

                                    $(scope.elementReference).css( {"zIndex":2,"position":"relative"});
                                    $(scope.backgroundElement).css( "zIndex",  $(scope.elementReference).css( "zIndex") -1);
                                }
                                $(scope.elementReference).focus();
                            }
                            else
                            {
                                $(scope.elementReference).removeClass('element-width-error');

                                if(typeof scope.newElementReference!== 'undefined')
                                {
                                    scope.newElementReference.remove();
                                }
                                if(scope.hasBackgroundError)
                                {
                                    scope.backgroundElement.remove();
                                    scope.hasBackgroundError = false;
                                }
                            }
                        },true);

                    });

                }
            };
}]);




/**
 * descripcion  error v2
 * @author      Benomas  (benomas@gmail.com) 2015
 * @access      dependencias    $timeout
 * @param       tipo parametros     -
 */

general.directive('errorDetail', function ()
{
    return {
                restrict:'E',
                scope:
                {
                },
                template:'<div class="alert-danger alert-error" role="alert" >'+
                            '<div class="alert" ng-bind-html="errorMessage()">'+
                            '</div>'+
                         '</div>',
                link: function(scope, element, attrs)
                {
                    scope.errorMessage = function()
                    {
                        return '<span class="errors-icon  glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>'+scope.$parent.error;
                    }

                    $(element.children()).hide();
                    $(scope.$parent.elementReference).mouseenter(function()
                    {
                        scope.$parent.recalculateDimension();
                        $(element.children()).show();
                    });

                    $(scope.$parent.elementReference).mouseleave(function()
                    {
                        $(element.children()).hide();
                    });

                    $(scope.$parent.elementReference).change(function()
                    {
                        $(element.children()).hide();
                    });
                }
            }
});

