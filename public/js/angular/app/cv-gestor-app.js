'use strict';

  var userAuth    = angular.module('userAuth',[]);
  var dashboard   =  angular.module("dashboard",[]);
  var curriculum  =  angular.module("curriculum",[]);
  var academic    =  angular.module("academic",[]);
  var work        =  angular.module("work",[]);
  var catalog     =  angular.module("catalog",[]);
  var general     =  angular.module("general",[]);

  var cvGestorApp =  angular.module("cvGestorApp",
	 [
		'ngRoute',
		'ngSanitize',
		'angular.filter',
		'userAuth',
		'dashboard',
		'curriculum',
		'academic',
		'work',
		'catalog',
		'general'
	 ]);

	 //----Routing & multiples Views
	 cvGestorApp.config(['$routeProvider',
	 function($routeProvider)
	 {
		$routeProvider.when('/dashboard/',
		{
		  templateUrl: angular_url + 'dashboard.html',
		  controller: 'dashboardController'
		})
		.otherwise(
		{
		  redirectTo: '/dashboard/'
		});
	 }]);

//Aberracion de IE- No guardar cache para Ajax.
cvGestorApp.config(['$httpProvider', function($httpProvider) {
	 //initialize get if not there
	 if (!$httpProvider.defaults.headers.get) {
		  $httpProvider.defaults.headers.get = {};
	 }

	 $httpProvider.interceptors.push(function ($q)
	 {
			 return {
				  'request': function (request)
				  {
						if(typeof request.data!=='undefined')
							request.data._token=api_token._token;
						/*else
						{
							request.data={"_token":api_token._token};
						}*/
						return request;
				  },
				  'response': function (response)
				  {
						return response;
				  },
				  'responseError': function (rejection)
				  {
				  		if(
				  				typeof rejection !== 'undefined' &&
				  				typeof rejection.data !== 'undefined' &&
				  				typeof rejection.data.data !== 'undefined' &&
				  				typeof rejection.data.data.custom_errors !== 'undefined'
				  			)
				  		{
				  			var newCustomError = {"error":{}};
				  			$.each(rejection.data.data.custom_errors,
			  				function(index,value)
			  				{
			  					newCustomError['error'][index]=value[0];
			  				});
			  				rejection.data.data=newCustomError;
			  				delete rejection.data.data.custom_errors;
				  		}
						return $q.reject(rejection);
				  }
			 };
	 });
	 //disable IE ajax request caching
	 $httpProvider.defaults.headers.get['If-Modified-Since'] = '0';
    $httpProvider.defaults.headers.common['X-CSRF-Token'] = api_token._token;
}]);

