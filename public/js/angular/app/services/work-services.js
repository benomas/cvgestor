// Factory, donde estan todos los servicios.

work.factory('workServices', ['$http', function($http)
{
    // api_url se define como variable global en vista main.blade
    var objectApi = 'work';
    var workServices = {};

    workServices.index = function ()
    {
        return $http.get(api_url + objectApi);
    };

    workServices.show = function (id)
    {
        return $http.get(api_url + objectApi+'/'+id);
    };

    workServices.store = function (data)
    {
        return $http.post(api_url + objectApi , data);
    };

    workServices.update = function (data,index,id)
    {
        return $http.put(api_url + objectApi + '/' +id, data);
    };

    workServices.destroy = function (id)
    {
        return $http.delete(api_url + objectApi+'/'+id);
    };

    workServices.index_by_curriculum = function (curriculum_id)
    {
        return $http.get(api_url + objectApi + '/curriculumId/' + curriculum_id);
    };

    return workServices;
}]);