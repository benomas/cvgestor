// Factory, donde estan todos los servicios.

academic.factory('academicServices', ['$http', function($http)
{
    // api_url se define como variable global en vista main.blade
    var objectApi = 'academic';
    var academicServices = {};

    academicServices.index = function ()
    {
        return $http.get(api_url + objectApi);
    };

    academicServices.show = function (id)
    {
        return $http.get(api_url + objectApi+'/'+id);
    };

    academicServices.store = function (data)
    {
        return $http.post(api_url + objectApi , data);
    };

    academicServices.update = function (data,index,id)
    {
        return $http.put(api_url + objectApi + '/' +id, data);
    };

    academicServices.destroy = function (id)
    {
        return $http.delete(api_url + objectApi+'/'+id);
    };

    academicServices.index_by_curriculum = function (curriculum_id)
    {
        return $http.get(api_url + objectApi + '/curriculumId/' + curriculum_id);
    };

    return academicServices;
}]);