// Factory, donde estan todos los servicios.

//var userAuth = angular.module('userAuth',[]);

userAuth.factory('userAuthService', ['$http', function($http)
{
    // api_url se define como variable global en vista main.blade
    var objectApi = 'user';
    var userAuthService = {};

    userAuthService.showCurrentUser = function ()
    {
        return $http.get(api_url + objectApi + '/currentUser');
    };

    userAuthService.index = function ()
    {
        return $http.get(api_url + objectApi);
    };

    userAuthService.show = function (id)
    {
        return $http.get(api_url + objectApi+'/'+id);
    };

    userAuthService.store = function (user)
    {
        return $http.post(api_url + objectApi+'/'+user);
    };

    userAuthService.update = function (datos)
    {
        return $http.put(api_url + objectApi+'/'+user);
    };

    userAuthService.destroy = function (id)
    {
        return $http.delete(api_url + objectApi+'/'+id);
    };

    return userAuthService;
}]);