// Factory, donde estan todos los servicios.

catalog.factory('rangeServices', ['$http', function($http)
{
    // api_url se define como variable global en vista main.blade
    var objectApi = 'range';
    var rangeServices = {};

    rangeServices.index = function ()
    {
        return $http.get(api_url + objectApi);
    };

    rangeServices.show = function (id)
    {
        return $http.get(api_url + objectApi+'/'+id);
    };

    rangeServices.store = function (data)
    {
        return $http.post(api_url + objectApi , data);
    };

    rangeServices.update = function (data,index,id)
    {
        return $http.put(api_url + objectApi + '/' +id, data);
    };

    rangeServices.destroy = function (id)
    {
        return $http.delete(api_url + objectApi+'/'+id);
    };

    return rangeServices;
}]);

catalog.factory('roleServices', ['$http', function($http)
{
    // api_url se define como variable global en vista main.blade
    var objectApi = 'role';
    var roleServices = {};

    roleServices.index = function ()
    {
        return $http.get(api_url + objectApi);
    };

    roleServices.show = function (id)
    {
        return $http.get(api_url + objectApi+'/'+id);
    };

    roleServices.store = function (data)
    {
        return $http.post(api_url + objectApi , data);
    };

    roleServices.update = function (data,index,id)
    {
        return $http.put(api_url + objectApi + '/' +id, data);
    };

    roleServices.destroy = function (id)
    {
        return $http.delete(api_url + objectApi+'/'+id);
    };

    return roleServices;
}]);