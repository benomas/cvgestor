<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middleware' => ['web']], function ()
{
    // Authentication routes...
    Route::get('auth/login', 'Auth\AuthController@getLogin');
    Route::post('auth/login', 'Auth\AuthController@postLogin');
    Route::get('auth/logout', 'Auth\AuthController@getLogout');

    // Registration routes...
    Route::get('auth/register', 'Auth\AuthController@getRegister');
    Route::post('auth/register', 'Auth\AuthController@postRegister');


    Route::get('auth/logout', 'Auth\AuthController@getLogout');
    Route::post('auth/logout', 'Auth\AuthController@postLogout');



    Route::get('/', function ()
    {
        return redirect('auth/login');
    });
});

Route::group(['middleware' => ['web','auth']], function ()
{
    Route::get('/', function ()
    {
        $jsTrans=array();
        $jsTrans["cvgestor"]    = trans('cvgestor');
        $jsTrans["auth"]        = trans('auth');
        $jsTrans["pagination"]  = trans('pagination');
        $jsTrans["passwords"]   = trans('passwords');
        $jsTrans["validation"]  = trans('validation');
        return view('main',['jsTrans' =>json_encode($jsTrans)]);
    });

    Route::resource('user/currentUser', 'UserController@currentUser');
    Route::resource('user', 'UserController');

    Route::resource('curriculum', 'CurriculumController');
    Route::get('curriculum/user_id/{user_id}', 'CurriculumController@indexByUserId');

    Route::resource('academic', 'AcademicController');
    Route::get('academic/curriculumId/{curriculum_id}', 'AcademicController@indexByCurriculumId');

    Route::resource('work', 'WorkController');
    Route::get('work/curriculumId/{curriculum_id}', 'WorkController@indexByCurriculumId');


    Route::resource('role', 'RolwController');
    Route::resource('range', 'RangeController');
});
