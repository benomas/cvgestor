<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Academic AS Academic;
use Auth;
use Validator;

class AcademicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(),
        [
            'academic_begin_year'   => 'required|integer|between:1800,2020',
            'academic_end_year'     => 'required|integer|between:1800,2020',
            'career'                => 'required|max:255',
            'curriculums_id'        => 'required|integer|exists:curriculums,id',
            'institute_name'        => 'required|max:255',
        ]);

        if ($validator->fails())
        {
            $validator_errors= $validator->errors();
            return response()->json(['status'=>'ok','data'=>array("custom_errors"=>$validator_errors)], 400);
        }

        $academic = new Academic;

        $academic->academic_begin_year    = $request->academic_begin_year;
        $academic->academic_end_year      = $request->academic_end_year;
        $academic->career                 = $request->career;
        $academic->curriculums_id         = $request->curriculums_id;
        $academic->institute_name         = $request->institute_name;
        $academic->save();
        return response()->json(['status'=>'ok','data'=>$academic], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),
        [
            'academic_begin_year'   => 'required|integer|between:1800,2020',
            'academic_end_year'     => 'required|integer|between:1800,2020',
            'career'                => 'required|max:255',
            'curriculums_id'        => 'required|integer|exists:curriculums,id',
            'institute_name'        => 'required|max:255',
        ]);

        if ($validator->fails())
        {
            $validator_errors= $validator->errors();
            return response()->json(['status'=>'ok','data'=>array("custom_errors"=>$validator_errors)], 400);
        }

        $academic = Academic::find($id);

        $academic->academic_begin_year    = $request->academic_begin_year;
        $academic->academic_end_year      = $request->academic_end_year;
        $academic->career                 = $request->career;
        $academic->curriculums_id         = $request->curriculums_id;
        $academic->institute_name         = $request->institute_name;
        $academic->save();
        return response()->json(['status'=>'ok','data'=>$academic], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Academic::destroy($id))
            return response()->json(['status'=>'ok','data'=>'success'], 200);
        return response()->json(['status'=>'ok','data'=>'error'], 400);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexByCurriculumId($curriculum_id)
    {
        $academics = Academic::academicsByCurriculum($curriculum_id);
        if($academics)
            return response()->json(['status'=>'ok','data'=>$academics], 200);

        return response()->json(['status'=>'error','data'=>'{"messages":{"general":"'.trans('cvgestor.no_login').'"}}'], 400);
    }
}
