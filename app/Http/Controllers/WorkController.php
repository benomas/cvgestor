<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Work AS Work;
use Auth;
use Validator;

class WorkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
        [
            'work_begin_year'   => 'required|integer|between:1800,2020',
            'work_end_year'     => 'required|integer|between:1800,2020',
            'company'           => 'required|max:255',
            'curriculums_id'    => 'required|integer|exists:curriculums,id',
            'work_position'     => 'required|max:255',
        ]);

        if ($validator->fails())
        {
            $validator_errors= $validator->errors();
            return response()->json(['status'=>'ok','data'=>array("custom_errors"=>$validator_errors)], 400);
        }

        $work = new Work;

        $work->work_begin_year      = $request->work_begin_year;
        $work->work_end_year        = $request->work_end_year;
        $work->company              = $request->company;
        $work->curriculums_id       = $request->curriculums_id;
        $work->work_position        = $request->work_position;
        $work->save();
        return response()->json(['status'=>'ok','data'=>$work], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),
        [
            'work_begin_year'   => 'required|integer|between:1800,2020',
            'work_end_year'     => 'required|integer|between:1800,2020',
            'company'           => 'required|max:255',
            'curriculums_id'    => 'required|integer|exists:curriculums,id',
            'work_position'     => 'required|max:255',
        ]);

        if ($validator->fails())
        {
            $validator_errors= $validator->errors();
            return response()->json(['status'=>'ok','data'=>array("custom_errors"=>$validator_errors)], 400);
        }

        $work = new Work;
        $work::find($id);

        $work->work_begin_year      = $request->work_begin_year;
        $work->work_end_year        = $request->work_end_year;
        $work->company              = $request->company;
        $work->curriculums_id       = $request->curriculums_id;
        $work->work_position        = $request->work_position;
        $work->save();
        return response()->json(['status'=>'ok','data'=>$work], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Work::destroy($id))
            return response()->json(['status'=>'ok','data'=>'success'], 200);
        return response()->json(['status'=>'ok','data'=>'error'], 400);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexByCurriculumId($curriculum_id)
    {
        $works = Work::worksByCurriculum($curriculum_id);
        if($works)
            return response()->json(['status'=>'ok','data'=>$works], 200);

        return response()->json(['status'=>'error','data'=>'{"messages":{"general":"'.trans('cvgestor.no_login').'"}}'], 400);
    }
}
