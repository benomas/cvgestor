<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Curriculum AS Curriculum;
use Auth;
use Validator;

class CurriculumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$curriculums = Curriculum::all();
        $curriculums = Curriculum::curriculumsWithUser();
        if($curriculums)
            return response()->json(['status'=>'ok','data'=>$curriculums], 200);

        return response()->json(['status'=>'error','data'=>'{"messages":{"general":"'.trans('cvgestor.no_login').'"}}'], 400);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(),
        [
            'age'                   => 'required|integer|between:1,200',
            'ranges_id'             => 'required|exists:ranges,id',
            'users_id'              => 'required|exists:users,id'
        ]);

        if ($validator->fails())
        {
            $validator_errors= $validator->errors();
            return response()->json(['status'=>'ok','data'=>array("custom_errors"=>$validator_errors)], 400);
        }


        $curriculum = new Curriculum;
        $curriculum->age          = $request->age;
        $curriculum->ranges_id    = $request->ranges_id;
        $curriculum->users_id     = $request->users_id;
        $curriculum->save();
        return response()->json(['status'=>'ok','data'=>$curriculum], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),
        [
            'age'                   => 'required|integer|between:1,200',
            'ranges_id'             => 'required|exists:ranges,id'
        ]);

        if ($validator->fails())
        {
            $validator_errors= $validator->errors();
            return response()->json(['status'=>'ok','data'=>array("custom_errors"=>$validator_errors)], 400);
        }

        $curriculum = Curriculum::find($id);

        $curriculum->age          = $request->age;
        $curriculum->ranges_id    = $request->ranges_id;
        $curriculum->save();
        return response()->json(['status'=>'ok','data'=>$curriculum], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexByUserId($user_id)
    {
        $curriculums = Curriculum::curriculumsWithUserByUser($user_id);
        if($curriculums)
            return response()->json(['status'=>'ok','data'=>$curriculums], 200);

        return response()->json(['status'=>'error','data'=>'{"messages":{"general":"'.trans('cvgestor.no_login').'"}}'], 400);
    }
}
