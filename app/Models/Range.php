<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class Range extends Model
{
    protected $table = 'ranges';

    public static function rangesAsCatalog()
    {
        return DB::table('ranges AS r')
                    ->select(   'r.id AS value',
                                'r.name AS label'
                            )->get();
    }
}
