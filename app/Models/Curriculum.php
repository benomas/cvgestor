<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class Curriculum extends Model
{
    protected $table = 'curriculums';

    public static function curriculumsWithUser()
    {
        return DB::table('curriculums AS c')
                    ->join('users AS u', 'c.users_id', '=', 'u.id')
                    ->join('ranges AS r', 'c.ranges_id', '=', 'r.id')
                    ->select(   'c.id',
                                'c.age',
                                'r.name AS range',
                                'c.ranges_id',
                                'u.name AS user',
                                'u.email'
                            )->get();
    }

    public static function curriculumsWithUserByUser($users_id)
    {
        return DB::table('curriculums AS c')
                    ->join('users AS u', 'c.users_id', '=', 'u.id')
                    ->join('ranges AS r', 'c.ranges_id', '=', 'r.id')
                    ->select(   'c.id',
                                'c.age',
                                'r.name AS range',
                                'c.ranges_id',
                                'u.name AS user',
                                'u.email'
                            )
                    ->where('c.users_id', '=', $users_id)
                    ->first();
    }
}
