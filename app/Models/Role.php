<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';


    public static function rolesAsCatalog()
    {
        return DB::table('roles AS r')
                    ->select(   'r.id AS value',
                                'r.name AS label'
                            )->get();
    }
}
