<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class Academic extends Model
{
    protected   $table = 'academics';
    public      $timestamps = true;


    public static function academicsByCurriculum($curriculums_id)
    {
        return DB::table('academics AS a')
                    ->select(   'a.id',
                                'a.academic_begin_year',
                                'a.academic_end_year',
                                'a.institute_name',
                                'a.career',
                                'a.curriculums_id'
                            )
                    ->where('a.curriculums_id', '=', $curriculums_id)->get();
    }

    public static function academicsById($id)
    {
        return DB::table('academics AS c')
                    ->select(   'a.id',
                                'a.academic_begin_year',
                                'a.academic_end_year',
                                'a.institute_name',
                                'a.career',
                                'a.curriculums_id'
                            )
                    ->where('a.id', '=', $id)
                    ->first();
    }
}
