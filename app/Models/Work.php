<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
    protected $table = 'works';


    public static function worksByCurriculum($curriculums_id)
    {
        return DB::table('works AS w')
                    ->select(   'w.id',
                                'w.work_begin_year',
                                'w.work_end_year',
                                'w.company',
                                'w.work_position',
                                'w.curriculums_id'
                            )
                    ->where('w.curriculums_id', '=', $curriculums_id)->get();
    }

    public static function worksById($id)
    {
        return DB::table('works AS w')
                    ->select(   'w.id',
                                'w.work_begin_year',
                                'w.work_end_year',
                                'w.company',
                                'w.work_position',
                                'w.curriculums_id'
                            )
                    ->where('a.id', '=', $id)
                    ->first();
    }
}
