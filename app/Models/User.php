<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class User extends Model
{
    public static function getUser($users_id)
    {
        return DB::table('users AS u')
                    ->join('roles AS r', 'u.roles_id', '=', 'r.id')
                    ->select(   'u.id',
                                'u.name AS user_name',
                                'u.email',
                                'u.roles_id',
                                'r.name AS role_name')
                    ->where('u.id', '=', $users_id)
                    ->first();
    }
}
